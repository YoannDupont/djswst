window.onload = function() {
    let fileInput = document.getElementById('fileInput');
    let fileDisplayArea = document.getElementById('fileDisplayArea');

    fileInput.addEventListener('change', function(e) {
        let file = fileInput.files[0];
        let textType = /text.*/;

        if (file.type.match(textType)) {
            var reader = new FileReader();

            reader.onload = function(e) {
            fileDisplayArea.innerText = reader.result;
            //splittext();
            }

            reader.readAsText(file);    
        } else {
            fileDisplayArea.innerText = "File not supported!"
        }
    });
}



function tokenize(text, delimiters) {
    if (delimiters === '') {
        return;
    }

    let queryDelim = delimiters.replace(/(.)/gi, "\\$1");
    let sepReg = new RegExp('([' + queryDelim + '])', 'g');

    let txt = text.replace(/<\/?[^>]+>/gi, "");
    txt = txt.replace(/</," ");
    txt = txt.replace(/>/," ");
    txt = txt.replace(sepReg, " $1 ");

    return txt.split(/\s+/);
}



function splittext(){
  let queryDelim = document.getElementById('delimID').value;

  if (queryDelim === '') {
    let vide='<small><span style="text-align:center; border: 1pt dotted #939393 ; padding: 1pt; margin-left: 0px; margin-right: 0px; background:red">Entrez les délimiteurs pour segmenter les mots...</span></small>';
    document.getElementById('placeholder').innerHTML = '';
    document.getElementById('placeholder').innerHTML += vide;
    return;
  }

  queryDelim = queryDelim.replace(/(.)/gi, "\\$1");
  // sepReg = new RegExp('([' + queryDelim + '])', 'g');
  /*-----------------------------*/
  DictionnaireSource = new Map();
  NBMOTTOTALSource = 0;
  NBMOTSource = 0;
  let vide = '<small><span style="text-align:center; border: 1pt dotted #939393 ; padding: 1pt; margin-left: 0px; margin-right: 0px; background:#FDBBD2">Calcul en cours</span></small>';
  document.getElementById('placeholder').innerHTML = vide;
  /* on recupere tt le texte */
  let allLines = document.getElementById('fileDisplayArea'); 
  let arrayOfLines = allLines.innerText.split("\n");
  for (let line of arrayOfLines) {
    tokens = tokenize(line.trim(), queryDelim);
    // console.log(tokens);
    for (let token of tokens) {
       if (token === '') {continue;}
       DictionnaireSource.set(token, (DictionnaireSource.get(token) ?? 0) + 1);
       NBMOTTOTALSource += 1;
    }
  }
  NBMOTSource = DictionnaireSource.size;
  vide='<small><span style="text-align:center; border: 1pt dotted #939393 ; padding: 1pt; margin-left: 0px; margin-right: 0px; background:#FDBBD2">Segmentation terminée : nombre d\'occurrences de mots : '+NBMOTTOTALSource+' / Nombre de mots différents : '+ NBMOTSource +'</span></small>';
  document.getElementById('placeholder').innerHTML = vide;
}



function afficheDico() {
  /* Affichage du dictionnaire */
    let resultFinal = "";
    let table = '';
    table += '<table align="center" class="myTable">';
    table += '<tr><th colspan="5"><b>Dictionnaire</b></th></tr>';
    table += '<tr>';
    table +='    <th width="50%">Mot</th>';
    table +='    <th width="50%">Fréquence</th>';
    table += '</tr>';
    let LISTEMOTS = Array.from(DictionnaireSource.keys()).sort(function(a,b){
        let x = DictionnaireSource.get(a);/*toLowerCase();*/
        let y = DictionnaireSource.get(b);/*.toLowerCase();*/
        return x < y ? 1 : x > y ? -1 : 0;
        });
    for (let mot of LISTEMOTS) {
        table +='<tr><td>'+mot+'</td><td>'+DictionnaireSource.get(mot)+'</td></tr>';
    }
    table +='</table>';
    resultFinal+=table;

  /* ------------------------- */
  document.getElementById('placeholder').innerHTML = '';
  vide='<small><span style="text-align:center; border: 1pt dotted #939393 ; padding: 1pt; margin-left: 0px; margin-right: 0px; background:#FDBBD2">Segmentation terminée (SOURCE : '+NBMOTTOTALSource+' occurrences / '+NBMOTSource+ ' formes) </span></small>';
  document.getElementById('page-analysis').innerHTML = resultFinal;
}



function concordance_v2(){
    let text = document.getElementById('fileDisplayArea').innerText.trim();
    let queryDelim = document.getElementById('delimID').value;
    let lepole = document.getElementById('poleID').value;

    if (text === "") {
        alert("Le texte est vide !");
        return;
    }

    if (lepole === "") {
        alert("Le pôle est vide !");
        return;
    }

    let tokens = tokenize(text, queryDelim);
    let motifPole = new RegExp("^" + lepole + "$", 'g');
    let tailleContexte = parseInt(document.getElementById('lgID').value ?? "10");

    document.getElementById('page-analysis').innerHTML = "";

    let table='';
    table += '<table align="center" class="myTable">';
    table += '<tr><th colspan="3"><b>Concordance...</b></th></tr><tr>';
    table +='    <th width="40%">contexte gauche</th>';
    table +='    <th width="20%">pôle</th>';
    table +='    <th width="40%">contexte droit</th>';
    table += '</tr>';

    let start = 0;
    let end = 0;
    for (var i = 0; i < tokens.length; i++) {
        if (tokens[i].search(motifPole) != -1) {
        start = Math.max(i - tailleContexte, 0);
        end = Math.min(i + tailleContexte, tokens.length);
        lc = tokens.slice(start, i);
        rc = tokens.slice(i+1, end+1);
        table += "<tr>";
        table += "<td>" + lc.join(' ') + "</td>";
        table += "<td>" + "<font color='red'>"+tokens[i]+"</font>";
        table += "</td>" + "<td>" + rc.join(' ') + "</td>";
        table += "</tr>";
        }
    }

    document.getElementById('page-analysis').innerHTML = table;
}
